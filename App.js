import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import WorkLogger from './src/components/WorkLogger';

export default class App extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <WorkLogger />
            </View>
        );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 20
  },
});
