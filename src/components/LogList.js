import React from 'react';
import { StyleSheet, Text, View, ScrollView, TouchableOpacity } from 'react-native';
import LogItem from './LogItem';

export default class LogList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentPage: 1,
            itemsToShow: this.props.visibleLogs,
            logs: this.props.logs
        };

        this.deleteLog = this.deleteLog.bind(this);
        this.seeMoreLogs = this.seeMoreLogs.bind(this);
    }

    deleteLog(id) {
        this.props.deleteMethod(id);
    }

    seeMoreLogs() {
        let currentPage = this.state.currentPage;
        currentPage++;
        this.setState({currentPage: currentPage});
    }

    render() {
        let logItems;
        if(this.state.logs) {
            logItems = this.state.logs.map((log, index) => {
                if(index < this.state.currentPage * this.state.itemsToShow) {
                    return (
                        <LogItem key={log.id} logItem={log} deleteMethod={this.deleteLog} />
                    );
                }
            });
        }

        let totalLogHours = 0;
        if(this.state.logs) {
            for(let i=0; i<this.state.logs.length; i++) {
                if(this.state.logs[i].duration) {
                    totalLogHours += Number(this.state.logs[i].duration);
                }
            }
        }

        let seeMoreButton;
        if(this.state.logs.length > this.state.currentPage * this.state.itemsToShow) {
            seeMoreButton =
                <TouchableOpacity onPress={this.seeMoreLogs} style={styles.seeMoreButton}>
                    <Text style={styles.seeMoreText}>See more...</Text>
                </TouchableOpacity>;
        }

        return (
            <View style={styles.container}>
                <Text>Total log for the day: {totalLogHours} hours</Text>
                <ScrollView style={styles.logListContainer}>
                    <View>{logItems}</View>
                </ScrollView>
                {seeMoreButton}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column'
    },
    addLogContainer: {
        flex: 1
    },
    logListContainer: {
        flex: 2
    },
    seeMoreButton: {
        alignSelf: 'stretch',
        padding: 5,
        marginTop: 5,
        marginBottom: 5,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2980b9'
    },
    seeMoreText: {
        color: 'white'
    }
});
