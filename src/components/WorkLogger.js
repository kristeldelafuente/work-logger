import React from 'react';
import uuid from 'uuid';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import LogList from './LogList';
import AddLog from './AddLog';

export default class WorkLogger extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            visibleLogs: 3,
            workLogs: [
                {
                    id: uuid.v4(),
                    duration: 1.0,
                    project: 'hydra',
                    projectDescription: 'Hydra',
                    remarks: 'Set up project'
                }, {
                    id: uuid.v4(),
                    duration: 3.0,
                    project: 'hydra',
                    projectDescription: 'Hydra',
                    remarks: 'Created models'
                }, {
                    id: uuid.v4(),
                    duration: 2.5,
                    project: 'dragon-p2p',
                    projectDescription: 'Dragon P2P',
                    remarks: 'Fixed Login page issue'
                }, {
                    id: uuid.v4(),
                    duration: 1.0,
                    project: 'hydra',
                    projectDescription: 'Hydra',
                    remarks: 'Set up project x'
                }, {
                    id: uuid.v4(),
                    duration: 3.0,
                    project: 'hydra',
                    projectDescription: 'Hydra',
                    remarks: 'Created models x'
                }, {
                    id: uuid.v4(),
                    duration: 2.5,
                    project: 'dragon-p2p',
                    projectDescription: 'Dragon P2P',
                    remarks: 'Fixed Login page issue x'
                }
            ],
            projects: [
                {
                    key: 'hydra',
                    description: 'Hydra'
                }, {
                    key: 'dragon-my',
                    description: 'Dragon MY'
                }, {
                    key: 'dragon-p2p',
                    description: 'Dragon P2P'
                }
            ]
        };

        this.addLog = this.addLog.bind(this);
        this.deleteLog = this.deleteLog.bind(this);
    }

    deleteLog(id){
        let workLogs = this.state.workLogs;
        let index = workLogs.findIndex(x => x.id === id);
        workLogs.splice(index, 1);
        this.setState({workLogs: workLogs});
    }

    addLog(newLog) {
        newLog.id = uuid.v4();

        let workLogs = this.state.workLogs;
        workLogs.unshift(newLog);
        this.setState({workLogs: workLogs});
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.addLogContainer}>
                    <AddLog projects={this.state.projects} addLog={this.addLog} />
                </View>
                <LogList style={styles.logListContainer} projects={this.state.projects} logs={this.state.workLogs} deleteMethod={this.deleteLog} visibleLogs={this.state.visibleLogs} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between'
    },
    addLogContainer: {
        flex: 1
    },
    logListContainer: {
        flex: 3
    }
});
