import React from 'react';
import { StyleSheet, Text, TextInput, View, TouchableOpacity, Picker } from 'react-native';

export default class AddLog extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            project: '',
            projectIndex: '',
            newLog: {}
        }

        this.addLog = this.addLog.bind(this);
    }

    addLog() {
        let hasError = true;
        const {duration, project, projectIndex, remarks} = this.state;
        
        if(duration === undefined || (duration !== undefined && !duration)) {
            this.setState({message: 'Please enter duration', messageStyle: 'errorText'});
        } else if (remarks === undefined || (duration !== undefined && !remarks)) {
            this.setState({message: 'Please enter remarks', messageStyle: 'errorText'});
        } else if (project === undefined || (project !== undefined && !project)) {
            this.setState({message: 'Please select a project', messageStyle: 'errorText'});
        } else {
            hasError = false;
            this.setState({message: ''});
        }

        if(!hasError) {
            let newLog = {
                duration: this.state.duration,
                project: this.state.project,
                projectDescription: this.props.projects[this.state.projectIndex - 1].description,
                remarks: this.state.remarks
            };

            this.setState({newLog: newLog}, function() {
                this.props.addLog(this.state.newLog);
            });
        }
    }

    render() {
        return (
            <View style={styles.addLog}>
                <Text style={styles.errorText}>{this.state.message}</Text>
                <TextInput placeholder="Duration" style={styles.addLogInput}
                           keyboardType='numeric'
                           onChangeText={duration => this.setState({duration})} />
                <Picker
                  style={styles.addLogInput}
                  selectedValue={this.state.project}
                  onValueChange={(itemValue, itemIndex) => this.setState({project: itemValue, projectIndex: itemIndex})}>
                  <Picker.Item label='Please select a project...' value='0' />
                  {this.props.projects.map((item, key)=>(
                    <Picker.Item label={item.description} value={item.key} key={key} />)
                    )}
                </Picker>
                <TextInput placeholder="Remarks" style={styles.addLogInput}
                          onChangeText={remarks => this.setState({remarks})} />
                <TouchableOpacity onPress={this.addLog} style={styles.addLogButton}>
                      <Text style={styles.addLogText}>Add Log</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    addLogInput: {
        alignSelf: 'stretch',
        color: '#252525',
        padding: 10,
        backgroundColor: '#FDFDFD',
        borderTopWidth:2,
        borderTopColor: '#ededed',
        width: 200
    },
    addLogButton: {
        alignSelf: 'stretch',
        padding: 10,
        marginTop: 10,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2980b9'
    },
    addLogText: {
        color: 'white'
    },
    errorText: {
        fontWeight: 'bold',
        color: 'red',
        textAlign: 'center'
    }
});
