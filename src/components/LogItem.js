import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';

export default class LogItem extends React.Component {
    constructor(props) {
        super(props);
        this.deleteLog = this.deleteLog.bind(this, this.props.logItem.id);
    }
    deleteLog(id) {
        //console.log(this, id);
        this.props.deleteMethod(id);
    }
    render() {
        return (
            <View style={styles.logItem} key={this.props.logItem.id}>
                <Text style={styles.logText}>{this.props.logItem.duration}h - {this.props.logItem.projectDescription}</Text>
                <Text style={styles.logText}>{this.props.logItem.remarks}</Text>
                <TouchableOpacity onPress={this.deleteLog} style={styles.logDelete}>
                    <Text style={styles.logDeleteText}>x</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    logItem: {
        position: 'relative',
        padding: 20,
        paddingRight: 100,
        borderBottomWidth:2,
        borderBottomColor: '#ededed'
    },
    logText: {
        paddingLeft: 20,
        borderLeftWidth: 10,
        borderLeftColor: '#E91E63'
    },
    logDelete: {
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2980b9',
        padding: 10,
        top: 10,
        bottom: 10,
        right: 10
    },
    logDeleteText: {
        color: 'white'
    }
});
