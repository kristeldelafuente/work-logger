import React from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import LogItem from './LogItem';

export default class LogList extends React.Component {
    deleteLog(id) {
        this.props.deleteMethod(id);
    }

    render() {
        let logItems;
        if(this.props.logs) {
            let visibleLogs = this.props.visibleLogs ? this.props.logs : this.props.logs;
            logItems = visibleLogs.map(log => {
                return (
                    <LogItem key={log.id} logItem={log} deleteMethod={this.deleteLog.bind(this)} />
                );
            });
        }

        return (
            <View>
                <ScrollView style={styles.logs}>
                {logItems}
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column'
    },
    addLogContainer: {
        flex: 1
    },
    logListContainer: {
        flex: 2
    }
});
